package org.abbtech.user.dto;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import org.abbtech.user.model.UserRole;

import java.util.List;

@Getter
public class UserDTO {
    private String username;
    @Email(message = "incorrect email format")
    private String email;
    private String password;
    @NotEmpty(message = "must be at least one role(ADMIN or USER!")
    private List<String> userRoles;
}
