package org.abbtech.user.dto;


import org.abbtech.user.model.User;
import org.abbtech.user.model.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CustomUserDetailDTO implements UserDetails {


    private String username;

    private String password;
    private final Set<? extends GrantedAuthority> authorities;

    public CustomUserDetailDTO(User user){
        this.username = user.getUsername();
        this.password = user.getPassword();
        Set<GrantedAuthority> auths = new HashSet<>();
        for (UserRole role : user.getUserRoles()) {
            auths.add(new SimpleGrantedAuthority(role.getRole().toUpperCase()));
        }
        this.authorities=auths;

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
