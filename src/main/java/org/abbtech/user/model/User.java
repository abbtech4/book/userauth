package org.abbtech.user.model;

import jakarta.persistence.*;

import lombok.*;

import java.util.List;
import java.util.UUID;


@Table(name = "user_info")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class User {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "UUID")
    private UUID id;


    @Column(name = "username", nullable = false, length = 100)
    private String username;


    @Column(name = "email", nullable = false, length = 100)
    private String email;


    @Column(name = "PASS", nullable = false, length = 100)
    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "userInfo")
    private List<UserRole> userRoles;



}