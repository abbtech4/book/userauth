package org.abbtech.user.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.abbtech.user.dto.AuthDTO;
import org.abbtech.user.dto.UserDTO;
import org.abbtech.user.exception.UserNotFoundException;
import org.abbtech.user.model.User;
import org.abbtech.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Tag(name = "auth server,everyone can access this api",description = "for validation the user accessed through the gateway first request comes here")
public class AuthController {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;

    @Operation(summary = "user registers and gets jwt ")
    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody @Valid UserDTO userDTO){
         return new ResponseEntity<>(userService.addUser(userDTO), HttpStatus.OK);
    }





    @Operation(summary = "logging operation")
    @PostMapping("/login")
    public String getToken(@RequestBody AuthDTO authDTO){
      Authentication authentication =
              authenticationManager
                      .authenticate(new UsernamePasswordAuthenticationToken(authDTO.getUsername(),authDTO.getPassword()));
       if (authentication.isAuthenticated()){
           return userService.generateToken(authDTO.getUsername());
       }else throw new UserNotFoundException("no such user registered");
    }

    @Operation(summary = "validation")
    @GetMapping("/validate")
    public String validateToken(@RequestParam(name = "token") String token){
        System.out.println(token);
         userService.validateToken(token);
         return "validate";
    }

}
