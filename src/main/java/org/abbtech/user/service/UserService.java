package org.abbtech.user.service;

import lombok.RequiredArgsConstructor;
import org.abbtech.user.dto.UserDTO;
import org.abbtech.user.exception.EmailAlreadyExistsException;
import org.abbtech.user.exception.UserNotFoundException;
import org.abbtech.user.model.User;
import org.abbtech.user.model.UserRole;
import org.abbtech.user.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;


    public String addUser(UserDTO userRequest) {
        if(userRepository.findByEmail(userRequest.getEmail()).isEmpty()){
            var userinfo = User.builder()
                    .email(userRequest.getEmail())
                    .username(userRequest.getUsername())
                    .password(passwordEncoder.encode(userRequest.getPassword()))
                    .build();
            userinfo.setUserRoles(userRequest.getUserRoles().stream()
                    .map(role -> UserRole.builder()
                            .userInfo(userinfo)
                            .role(role).build())
                    .collect(Collectors.toList()));

            userRepository.save(userinfo);
            return jwtService.generateToken(userinfo);
        }
        throw new EmailAlreadyExistsException("Provided email already registered to the system!");
    }


    public String generateToken(String username){
        User user = userRepository.findByUsername(username).orElseThrow(()->new UserNotFoundException("no such user"));
        return jwtService.generateToken(user);
    }

    public void validateToken(String token){
        jwtService.validateToken(token);
    }
}
