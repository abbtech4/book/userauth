package org.abbtech.user.service;

import lombok.RequiredArgsConstructor;
import org.abbtech.user.dto.CustomUserDetailDTO;
import org.abbtech.user.exception.UserNotFoundException;
import org.abbtech.user.model.User;
import org.abbtech.user.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;



@RequiredArgsConstructor
@Component
public class CustomUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       User user = userRepository.findByUsername(username).orElseThrow(()->new UserNotFoundException("user not found"));
        return new CustomUserDetailDTO(user);
    }
}
